from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///relationships_many.db'
db = SQLAlchemy(app)

subs = db.Table('subs',
    db.Column('user_id', db.Integer, db.ForeignKey('user.user_id')),
    db.Column('home_id', db.Integer, db.ForeignKey('home.home_id'))
)

class User(db.Model):
    user_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))
    liviner = db.relationship('Home', secondary=subs, backref=db.backref('livening', lazy='dynamic'))

class Home(db.Model):
    home_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))

    