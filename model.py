# import sqlalchemy
# print(sqlalchemy.__version__)

from sqlalchemy import create_engine, declarative_base, Column, Integer,String
import pyodbc
engine = create_engine('mssql+pyodbc://sa:FEACDE@AISSORA/sht_db_new', echo = True) # echo включение журнала при помощи logging модуля Python

Base = declarative_base()

class Doc(Base):
    __tablename__ = 'slv_sotr'

    id = Column(Integer, primary_key=True)
    user_name = Column(String(10))
    password = Column(String(10))
    tf = Column(String(12))
    faks = Column(String(12))
    e_mail = Column(String(30))
    sotr = Column(String(30))
    kabinet = Column(String(10))
    id_group = Column(Integer)
    is_block = Column(Integer)
    id_group = Column(Integer)

    def __repr__(self):
        return 'Sotr(user={}, pass={}, tel={}, e_mail={}, sotr={}, is_block={}, id_group={})'\
            .format(self.user_name, self.password, self.tf, self.e_mail, self.sotr, self.is_block, self.id_group)

