from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///schema.db'
db = SQLAlchemy(app)
migrate = Migrate(app, db)

class Office(db.Model):
    """Подразделения (орган внутренних дел)"""
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20)) # Название подразделения
    # TODO: Название подразделения должно забираться и регулярно сверятся с адресно-телефонным справочником на ВИСПе

    employees = db.relationship('Users', backref='employee')

class Users(db.Model):
    """Участковые. Логин это email"""
    id = db.Column(db.Integer, primary_key=True)
    FIO = db.Column(db.String(100)) # Фамилия Имя Отчество
    dolz = db.Column(db.String(100)) # Должность
    # TODO: Название должности должно забираться и регулярно сверятся с адресно-телефонным справочником на ВИСПе
    office_id = db.Column(db.Integer, db.ForeignKey('office.id')) # Идентификатор подразделения
    email = db.Column(db.String(100)) # Почта (СЭП). Используется как логин
    # TODO: Почта должна регулярно сверятся с адресно-телефонным справочником на ВИСПе
    is_active = db.Column(db.Boolean, default=True) # Если сверка с адресно-телефонным справочником на ВИСПе не удачна, ставим False

    admin_ter = db.relationship('Adm_ter', backref='admin') 
    check_by = db.relationship('Checks', backref='check')

class Adm_ter(db.Model):
    """ Административный участок """
    id = db.Column(db.Integer, primary_key=True)
    num_ter = db.Column(db.Integer) # Номер участка
    date_add = db.Column(db.DateTime) # Дата Закрепления
    doc = db.Column(db.String(10)) # Приказ о закреплении
    square = db.Column(db.String(10)) # Площадь участка
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    homes = db.relationship('Home', backref='adm_home')

class Types(db.Model):
    """ Типы данных используемых для обозначения объектов (тип населенного пункта - город, село; тип дома - многоквартирный, 
    частный, и так далее)  """
    id = db.Column(db.Integer, primary_key=True)
    num_types = db.Column(db.Integer) # Номер типов (1-это все типы домов, 2 - все типы населенных пунктов и т.д)
    name_type = db.Column(db.String(50)) # Названия типов

    t_home = db.relationship('Home', backref='type_home')
    t_apartament = db.relationship('Apartament', backref='type_apartaments')
    # FIXME: Обратные отношения к Location должны как-то настраиваться, но не хватает знания, поэтому сначала запрашиваем Location, 
    # потом по id из Types запрашиваем отдельно name_type

class Location(db.Model):
    """ Местонахождение дома, включает в себя страну, нас. пункт, район, улицу """
    id = db.Column(db.Integer, primary_key=True)
    state = db.Column(db.Integer) # Страна числовой код(589 - Россия)
    region_name = db.Column(db.String(70)) # Название региона
    region_type_id = db.Column(db.Integer, db.ForeignKey('types.id')) # Тип региона (числовой код 1 (республика, область и др))
    town_name = db.Column(db.String(70)) # Название населенного пункта
    town_type_id = db.Column(db.Integer, db.ForeignKey('types.id')) # Тип населенного пункта (числовой код 2 (город, село, деревня и др))
    raion_name = db.Column(db.String(70)) # Название района субъекта федерации
    street_name = db.Column(db.String(70)) # Название улицы
    street_type_id = db.Column(db.Integer, db.ForeignKey('types.id')) # Тип улицы (числовой код 3 (проспект, улица, переулок и др.))

    region_types = db.relationship('Types', foreign_keys='[Location.region_type_id]')
    town_types = db.relationship('Types', foreign_keys='[Location.town_type_id]')
    street_type = db.relationship('Types', foreign_keys='[Location.street_type_id]')
    l_home = db.relationship('Home', backref='loc_home')


class Zone(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100)) # Название зоны

    z_homes = db.relationship('Home', backref='zone_home')

class Home(db.Model):
    """ Описание дома """
    id = db.Column(db.Integer, primary_key=True)
    num = db.Column(db.Integer) # Номер дома
    korp = db.Column(db.String(2), nullable=True) # Корпус 
    type_id = db.Column(db.Integer, db.ForeignKey('types.id')) # Тип дома(числовой код 4 (многоквартирный, частный))
    location_id = db.Column(db.Integer, db.ForeignKey('location.id')) # Тип местонахождения
    count_apartament = db.Column(db.Integer, nullable=True) # Количество квартир
    count_entrance = db.Column(db.Integer, nullable=True) # Количество подъездов
    count_flats = db.Column(db.Integer, nullable=True) # Количество этажей
    adm_ter_id = db.Column(db.Integer, db.ForeignKey('adm_ter.id')) # Идентификатор административного участка
    zone_id = db.Column(db.Integer, db.ForeignKey('zone.id')) # Идентификатор зоны

    org_in_home = db.relationship('Organization', backref='org_home')
    apartaments_home = db.relationship('Home', backref='apartaments')

class Checks(db.Model):
    """ Проверки квартир и организациий находящихся в доме """
    id = db.Column(db.Integer, primary_key=True)
    date_add = db.Column(db.DateTime) # Дата Проверки
    result = db.Column(db.Text) # Результат проверки
    user_id = db.Column(db.Integer, db.ForeignKey('user.id')) # Идентификатор участкового проводивщего проверку

    checked_org = db.relationship('Organization', backref='check_org')
    checked_apart = db.relationship('Organization', backref='ch_apartament')
    checked_citizen = db.relationship('Citizen', backref='ch_citizen')

class Organization(db.Model):
    """ Организации находящиеся в доме """
    id = db.Column(db.Integer, primary_key=True)
    home_id = db.Column(db.Integer, db.ForeignKey('home.id')) # идентификатор дома, в котором находится организация
    name = db.Column(db.String(200)) # Название организации
    check_id = db.Column(db.Integer, db.ForeignKey('checks.id')) # Идентификатор проверки организации
    is_active = db.Column(db.Boolean, default=True) # Работающая ли организация

fact_live = db.Table('fact_live',
    db.Column('apartament_id', db.Integer, db.ForeignKey('apartament.apartament_id')),
    db.Column('citizen_id', db.Integer, db.ForeignKey('Citizen.citizen_id'))
)

check_citizen = db.Table('check_citizen',
    db.Column('check_id', db.Integer, db.ForeignKey('Type_check.check_id')),
    db.Column('citizen_id', db.Integer, db.ForeignKey('Citizen.citizen_id'))
)

class Apartament(db.Model):
    """ Описание квартиры """
    apartament_id = db.Column(db.Integer, primary_key=True)
    home_id = db.Column(db.Integer, db.ForeignKey('home.id')) # идентификатор дома, в котором находится квартира
    num_apartrament = db.Column(db.Integer) # Номер квартиры
    num_entrance = db.Column(db.Integer) # Номер подъезда
    num_flat = db.Column(db.Integer) # Номер этажа
    type_id = db.Column(db.Integer, db.ForeignKey('types.id')) # Тип квартиры (притон, нормальная квартира и др.)
    check_id = db.Column(db.Integer, db.ForeignKey('checks.id')) # Идентификатор проверки квартиры



class Type_check(db.Model):
    """ По каким учетам проходит лицо (Наркоман, Адм.Надзор и др.) """
    check_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250)) # Название учета

    check_cit = db.relationship('Citizen', secondary=check_citizen, backref=db.backref('checks_citizen', lazy='dynamic'))


class Citizen(db.Model):
    """ Что за лица проживают либо зарегистрированы в квартире """
    citizen_id = db.Column(db.Integer, primary_key=True)
    home_id = db.Column(db.Integer, db.ForeignKey('home.id')) # идентификатор дома, в котором зарегистрирован либо проживает
    FIO =  db.Column(db.String(200)) # Фамилия имя отчество
    born = db.Column(db.DateTime) # Дата рождения
    tel = db.Column(db.String(20)) # Телефон
    num_pass = db.Column(db.String(20)) # Номер паспорта
    gra = db.Column(db.String(20)) # Гражданство
    # apartament_id = db.Column(db.Integer, db.ForeignKey('apartament.id')) #
    work = db.Column(db.String(200)) # Место работы
    look = db.Column(db.Text) # Обстановка в квартире
    check_id = db.Column(db.Integer, db.ForeignKey('checks.id')) # Дата проверки
    is_reg = db.Column(db.Boolean, default=True) # Проживает ли по месту регистрации
    
    liviner = db.relationship('Apartament', secondary=fact_live, backref=db.backref('livening', lazy='dynamic'))